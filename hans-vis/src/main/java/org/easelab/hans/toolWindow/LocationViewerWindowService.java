package org.easelab.hans.toolWindow;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.project.Project;

/**
 * Service providing the LocationViewer
 * @see LocationViewerWindow
 */
public class LocationViewerWindowService implements Disposable {

    public LocationViewerWindow locationViewerWindow;

    public LocationViewerWindowService(Project project){
        locationViewerWindow = new LocationViewerWindow(this, project);
    }
    @Override
    public void dispose() { locationViewerWindow = null;}
}
