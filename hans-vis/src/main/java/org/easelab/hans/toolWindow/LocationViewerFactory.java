package org.easelab.hans.toolWindow;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class LocationViewerFactory implements ToolWindowFactory {

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        LocationViewerWindow locationViewerWindow = ServiceManager.getService(project, LocationViewerWindowService.class).locationViewerWindow;
        JComponent component = toolWindow.getComponent();
        component.getParent().add(locationViewerWindow.content(), 0);
    }
}
