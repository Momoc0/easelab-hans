package org.easelab.hans.toolWindow;

import com.google.gson.Gson;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Disposer;
import com.intellij.openapi.vfs.CharsetToolkit;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.ui.JBColor;
import com.intellij.ui.jcef.JBCefBrowser;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.browser.CefMessageRouter;
import org.cef.callback.CefQueryCallback;
import org.cef.handler.CefMessageRouterHandler;
import se.ch.HAnS.fileAnnotation.FileAnnotationUtil;
import se.ch.HAnS.folderAnnotation.FolderAnnotationUtil;
import se.ch.HAnS.folderAnnotation.psi.FolderAnnotationLpq;
import se.ch.hans.annotations.iFeatureAnnotationUtil;
import se.ch.hans.toolwindow.JSONHandler;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.*;

public class LocationViewerWindow {

    private static LocationViewerWindow view;

    private final JBCefBrowser cefBrowser;
    private final Project project;
    private final Box content;
    private String locationFeature = "";
    private String projectName = "";
    private static String OS = System.getProperty("os.name").toLowerCase();
    public static boolean IS_WINDOWS = (OS.contains("win"));
    public static boolean IS_MAC = (OS.contains("mac"));
    public static boolean IS_UNIX = (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
    public static boolean IS_SOLARIS = (OS.contains("sunos"));

    public LocationViewerWindow(LocationViewerWindowService service, Project project) {
        // Set up JCEF
        cefBrowser = new JBCefBrowser();
        handleJS(cefBrowser.getCefBrowser().getClient());
        registerAppSchemeHandler();
        cefBrowser.loadURL("http://myapp/html/echartsView.html");
        Disposer.register(service, cefBrowser);

        // Setup the menubar in JCEF
        JMenuBar menuBar = new JMenuBar();
        JMenuItem foMappingsMenuItem = new JMenuItem("Folder Location Visualization");
        JMenuItem fiMappingsMenuItem = new JMenuItem("File Location Visualization");
        foMappingsMenuItem.setHorizontalTextPosition(SwingConstants.CENTER); // set item in the middle
        fiMappingsMenuItem.setHorizontalTextPosition(SwingConstants.CENTER); // set item in the middle

        JBColor menuItemColor = new JBColor(
                foMappingsMenuItem.getBackground(), foMappingsMenuItem.getBackground());
        MouseAdapter miMouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String url = "http://myapp/html/";
                if (e.getComponent() == foMappingsMenuItem) {
                    url += "echartsView.html";
                } else if (e.getComponent() == fiMappingsMenuItem) {
                    url += "echartsView1.html";
                }
                cefBrowser.loadURL(url);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                e.getComponent().setBackground(JBColor.CYAN);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                e.getComponent().setBackground(menuItemColor);
            }
        };
        foMappingsMenuItem.addMouseListener(miMouseAdapter);
        fiMappingsMenuItem.addMouseListener(miMouseAdapter);

        // Add config-stuffs into component
        menuBar.add(foMappingsMenuItem);
        menuBar.add(fiMappingsMenuItem);
        content = new Box(BoxLayout.Y_AXIS);
        content.add(menuBar);
        content.add(cefBrowser.getComponent());
        this.project = project;
    }

    /**
     * Gets the JCEF browser
     *
     * @return The JCEF browser as a JComponent.
     */
    public JComponent content() {
        return content;
    }

    /**
     * Register a new SchemeHandlerFactory using CustomSchemeHandlerFactory
     */
    private void registerAppSchemeHandler() {
        CefApp.getInstance().registerSchemeHandlerFactory(
                "http",
                "myapp",
                new CustomSchemeHandlerFactory()
        );
    }

    public void handleJS(CefClient client) {
        // Query a route configuration, html pages used window.java ({}) and window.javaCancel ({}) to call this method
        CefMessageRouter.CefMessageRouterConfig cmrConfig = new CefMessageRouter.CefMessageRouterConfig(
                "java", "javaCancel");
        // Create query routing
        CefMessageRouter cmr = CefMessageRouter.create(cmrConfig);

        cmr.addHandler(new CefMessageRouterHandler() {

            @Override
            public void setNativeRef(String str, long val) {
                System.out.println(str + "  " + val);
            }

            @Override
            public long getNativeRef(String str) {
                System.out.println(str);
                return 0;
            }

            @Override
            public void onQueryCanceled(CefBrowser browser, CefFrame frame, long query_id) {
                System.out.println("cancel query :" + query_id);
            }

            @Override
            public boolean onQuery(CefBrowser browser, CefFrame frame, long query_id, String request, boolean persistent,
                                   CefQueryCallback callback) {
                System.out.println("request:" + request + "\nquery_id:" + query_id + "\npersistent:" + persistent);

                String[] temp = request.split(" ");
                String cmd = temp[0];
                boolean handled = false;
                String featureLocation = "";
                Gson gson = new Gson();
                switch (cmd) {
                    case "FoMappingDataFromBackend":
                        try {
                            if (locationFeature.equals("")) {
                                projectName = CustomResourceHandler.getCurrentProjectName();
                                // fetch "feature-to-folder" locations
                                Map<FolderAnnotationLpq, PsiDirectory> foMappings = FolderAnnotationUtil.findAllFolderMappings(project);
                                featureLocation = JSONHandler.HAnS2JSONString(foMappings);
//                                System.out.println("[featureLocation in JSONString] " + featureLocation); // helper method can be removed
                            }
                            callback.success(featureLocation);
                            handled = true;
                        } catch (IndexOutOfBoundsException e) {
                            callback.failure(400, "Could not get feature mappings.");
                        }
                        break;
                    case "DeleteFo":
                        try {
                            String featureName = temp[1];
                            //***************************************************
                            Map<FolderAnnotationLpq, PsiDirectory> foMapping = FolderAnnotationUtil.findFolderMapping(project, featureName);
                            PsiDirectory psiDir = null;
                            for (Map.Entry entry : foMapping.entrySet()) { // find the Psi path to the folder
                                psiDir = (PsiDirectory) entry.getValue();
                                break;
                            }
                            PsiFile[] children = psiDir.getFiles();
                            for (PsiFile child : children) { // look for the '.feature-to-folder' file in the current folder
                                if (child.getName().equals(".feature-to-folder")) {
                                    VirtualFile vFile = child.getVirtualFile();
                                    String contentsToString = new String(vFile.contentsToByteArray());
                                    String modContent = contentsToString.replaceAll(featureName, ""); // remove feature name from current file
                                    ApplicationManager.getApplication().invokeLater(() -> {
                                        try {
                                            vFile.setBinaryContent(modContent.getBytes());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    });
                                    break;
                                }
                            }
                            //***************************************************
                            callback.success(featureName + " was deleted!");
                            handled = true;
                        } catch (IndexOutOfBoundsException | IOException e) {
                            callback.failure(400, "DeleteFo error.");
                        }
                        break;
                    case "RenameFo":
                        try {
                            String oldName = temp[1];
                            String newName = temp[2];
                            System.out.println("[oldName] " + oldName + " [newName] " + newName);
                            //***************************************************
                            Map<FolderAnnotationLpq, PsiDirectory> foMapping = FolderAnnotationUtil.findFolderMapping(project, oldName);
                            PsiDirectory psiDir = null;
                            for (Map.Entry entry : foMapping.entrySet()) {
                                psiDir = (PsiDirectory) entry.getValue();
                                break;
                            }
                            PsiFile[] children = psiDir.getFiles();
                            for (PsiFile child : children) {
                                if (child.getName().equals(".feature-to-folder")) {
                                    VirtualFile vFile = child.getVirtualFile();
                                    String contentsToString = new String(vFile.contentsToByteArray());
                                    String modContent = contentsToString.replace(oldName, newName);
                                    ApplicationManager.getApplication().invokeLater(() ->
                                    {
                                        try {
                                            vFile.setBinaryContent(modContent.getBytes(CharsetToolkit.UTF8_CHARSET));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    });
                                    break;
                                }
                            }
                            //***************************************************
                            callback.success(oldName + " was renamed to " + newName);
                            handled = true;
                        } catch (IndexOutOfBoundsException | IOException e) {
                            // Incorrect syntax
                            callback.failure(400, "RenameFo error.");
                        }
                        break;
                    case "AddFo":
                        String nodeName = temp[1];
                        String nodeFolderDir = temp[2];
                        String rootDir = "";
                        //***************************************************
                        if (IS_MAC) {
                            rootDir = System.getProperty("user.home");
                        }
                        if (IS_WINDOWS) {
                            rootDir = System.getProperty("user.dir");
                        }
                        //TODO: parse the given folder directory.
                        //***************************************************
                        callback.success(nodeName + " was added to folder " + nodeFolderDir);
                        handled = true;
                        break;
                    case "FiMappingDataFromBackend":
                        try {
                            if (locationFeature.equals("")) {
                                projectName = CustomResourceHandler.getCurrentProjectName();
                                // fetch "feature-to-file" locations
                                Map<String, String> fiMappings = FileAnnotationUtil.featuretofile(project);
                                featureLocation = gson.toJson(fiMappings);
                                System.out.println("Files exist" + featureLocation);
                            }
                            callback.success(featureLocation);
                            handled = true;
                        } catch (IndexOutOfBoundsException e) {
                            callback.failure(400, "Could not get feature mappings.");
                        }
                        break;
                    case "DeleteFi": {
                        String featureName = temp[1];
                        //***************************************************
                        // TODO: implement this case for FiMappings
                        //***************************************************
                        callback.success(featureName + " was deleted!");
                        handled = true;
                    }
                        break;
                    case "RenameFi":
                        String oldName = temp[1];
                        String newName = temp[2];
                        //***************************************************
                        // TODO: implement this case for FiMappings
                        //***************************************************
                        callback.success(oldName + " was renamed to " + newName);
                        handled = true;
                        break;
                    case "AddFi":
                        String featureName = temp[1];
                        String fileDir = temp[2];
                        //***************************************************
                        if (IS_MAC) {
                            rootDir = System.getProperty("user.home");
                        }
                        if (IS_WINDOWS) {
                            rootDir = System.getProperty("user.dir");
                        }
                        // TODO: implement this case for FiMappings
                        // TODO: parse the given file directory.
                        //***************************************************
                        callback.success(featureName + " was added to file " + fileDir);
                        handled = true;
                        break;
                }
                return handled;
            }
        }, true);
        client.addMessageRouter(cmr);
    }

    public static LocationViewerWindow getView() {
        return view;
    }
}
