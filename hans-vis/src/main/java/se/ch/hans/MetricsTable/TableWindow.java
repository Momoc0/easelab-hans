package se.ch.hans.MetricsTable;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.table.JBTable;
import se.ch.hans.feature.FeatureService;
import se.ch.hans.misc.Feature;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;


/**
 * A Class that creates the table, that should be shown
 */
public class TableWindow {

    private JBTable table;
    private JBScrollPane panel;


    /**
     * Constructs a table, with the metrics
     */
    public TableWindow() {
        String[] columns = getColumns();


        /** Gets an instance of the class FeatureService which is capable of getting the features from Hans-Text**/
        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        FeatureService fs = new FeatureService(project);
        List<Feature> list = fs.getAllFeatures();

        Object[][] data = getMetrics (list);


        // Table
        DefaultTableModel model = new DefaultTableModel(data, columns);
        table = new JBTable(model);
        table.setCellSelectionEnabled(true);
        panel = new JBScrollPane(table);

    }

    /**
     *
     * @return the component which should be shown
     */
    public JComponent content() { return panel;}

    /**
     *
     * @return an Array with the titles of the metrics
     */
    public String[] getColumns ()
    {
        String [] metrics = {"Feature", "Scattering Degree",
        "Number of Code Annotations",
        "Number of File Annotations",
        "Number of Folder Annotations",
        "Tangling Degree",
        "Lines of Feature Code"};
        return metrics;
    }

    /**
     * The metrics of the features are stored in an array, so that they represents the cells of a table
     * @param list The features of which the metrics should be shown
     * @return cells of the table
     */
    public Object[][] getMetrics (List<Feature> list)
    {
        int numberMetrics = getColumns().length;
        Object[][] metrics = new Object [list.size()][numberMetrics];
        for (int i = 0; i<list.size(); i++)
        {
            Feature fs = list.get(i);
            metrics [i][0]= fs.name;
            metrics [i][1]= fs.sd;
            metrics [i][2]= fs.noca;
            metrics [i][3]= fs.nofia;
            metrics [i][4]= fs.nofoa;
            metrics [i][5]= fs.td;
            metrics [i][6]= fs.lofc;
        }
        return metrics;
    }

}
