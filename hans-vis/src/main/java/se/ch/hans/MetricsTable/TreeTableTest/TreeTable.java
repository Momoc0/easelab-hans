package se.ch.hans.MetricsTable.TreeTableTest;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.ui.components.JBScrollPane;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import se.ch.HAnS.featureModel.toolWindow.FeatureView;
import se.ch.hans.feature.FeatureService;
import se.ch.hans.misc.Feature;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.util.Arrays;
import java.util.List;

public class TreeTable
{

    private String [] metrics = {"Feature",
            "Scattering Degree",
            "Number of Code Annotations",
            "Number of File Annotations",
            "Number of Folder Annotations",
            "Tangling Degree",
            "Lines of Feature Code"};
    private RootNode root;
    private JXTreeTable table;
    private JBScrollPane panel;

    public TreeTable (){
    }

    //Creates a JXTreeTable by using the method getTreeTable and adjusting some settings
    public JXTreeTable getTreeTable () {
        table = createTreeTable();
        table.setShowGrid(true, true);
        table.setColumnControlVisible(true);
        table.setCellSelectionEnabled(true);
        table.packAll();
        panel = new JBScrollPane(table);
        return table;
    }

    //Creates a JXTreeTable by using the root from featureView
    private JXTreeTable createTreeTable ()
    {
        JXTreeTable treeTable;
        root = new RootNode (" Root ");
        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        FeatureView featureView = new FeatureView(project);

        for (int i=0;i<featureView.getRoot().getChildCount(); i++)
        {
            TreeNode child = featureView.getRoot().getChildAt(i);
            String nodeName = child.toString();
            Object [] treeNodeData = createData (nodeName);
            ChildNode treeTableChild = new ChildNode (treeNodeData);
            root.add(treeTableChild);
            createChildNode(treeTableChild, child);
        }
        DefaultTreeTableModel model = new DefaultTreeTableModel(root, Arrays.asList(metrics));
        treeTable = new JXTreeTable(model);
        return treeTable;
    }

    //Traverses the Tree from FeatureView and creates corresponding TreeTableNodes
   private void createChildNode (ChildNode child, TreeNode treeNode) {

        for (int i=0; i<treeNode.getChildCount(); i++)
        {
            TreeNode children = treeNode.getChildAt(i);
            Object [] data = createData(treeNode.getChildAt(i).toString());
            ChildNode temp = new ChildNode(data);
            createChildNode(temp, children);
            child.add(temp);
        }
    }


    //Creates the content of the Nodes, by adding the metrics, with help from FeatureService
    private Object []  createData (String feature){
        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        FeatureService fs = new FeatureService(project);
        List <Feature> featureList = fs.getAllFeatures();
        for (Feature feature1 : featureList) {
            if (feature1.name.equals(feature))
            {
                Object [] data = {feature,
                        feature1.sd,
                        feature1.noca,
                        feature1.nofia,
                        feature1.nofoa,
                        feature1.td,
                        feature1.lofc};
                return data;
            }
        }
        Object [] data = {feature};
        return data;
    }

    public JComponent content (){return panel;}
}