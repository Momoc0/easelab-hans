package se.ch.hans.MetricsTable.TreeTableTest;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import org.jetbrains.annotations.NotNull;


import javax.swing.*;
import java.awt.*;


public class ShowTreeTable extends AnAction
{

    @Override
    public void actionPerformed(@NotNull AnActionEvent e)
    {
        JFrame testFrame = new JFrame();

        TreeTable treeTable = new TreeTable();

        testFrame.setSize (500, 500);

        testFrame.setLayout(new BorderLayout());

        testFrame.add (new JScrollPane(treeTable.getTreeTable()), BorderLayout.CENTER);

        testFrame.setVisible(true);


    }
}
