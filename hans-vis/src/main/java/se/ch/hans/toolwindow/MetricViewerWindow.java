// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.toolwindow;

import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.ui.popup.JBPopup;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.PopupChooserBuilder;
import com.intellij.refactoring.rename.RenameDialog;
import com.intellij.testFramework.JavaModuleTestCase;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.components.JBTextField;
import com.intellij.ui.table.JBTable;
import com.intellij.util.ui.FormBuilder;
import org.jetbrains.annotations.NotNull;
import se.ch.HAnS.featureModel.psi.FeatureModelFeature;
import se.ch.hans.annotations.iFeatureAnnotationUtil;
import se.ch.hans.feature.FeatureService;
import se.ch.hans.misc.Feature;
import se.ch.hans.settings.SettingsState;
import com.intellij.codeInsight.navigation.NavigationUtil;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.table.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * A class creating the table of metrics used as content in a ToolWindow
 */
public class MetricViewerWindow {

    private final ArrayList<Feature> features;
    private final ArrayList<String> columnToolTips;
    private final JPanel panel;
    private final JBTable table;
    private final TableRowSorter<DefaultTableModel> sorter;
    private Popup popupForRightClick;

    /**
     * Constructs a metric view
     */
    public MetricViewerWindow() {
        // Data
        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        features = new ArrayList<>();
        FeatureService fs = ServiceManager.getService(project, FeatureService.class);
        features.addAll(fs.getAllFeatures());

        // Table
        columnToolTips = createColumnToolTips(getTableMetrics());
        DefaultTableModel model = createModel(features);
        table = new JBTable(model) {
            @Override
            protected @NotNull JTableHeader createDefaultTableHeader() {
                return new JTableHeader(columnModel) {
                    @Override
                    public String getToolTipText(MouseEvent event) {
                        int index = columnModel.getColumnIndexAtX(event.getPoint().x);
                        int realIndex = columnModel.getColumn(index).getModelIndex()-1;
                        return realIndex >= 0 ? columnToolTips.get(realIndex) : null;
                    }
                };
            }
        };
        table.getTableHeader().setReorderingAllowed(false);

        /**
         * Add right-click functionality to the metric viewer
         * @author: yifeng he
         */
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (popupForRightClick != null) {
                    popupForRightClick.hide();
                }

                if (e.getButton() == e.BUTTON3) {
                    // Get (x, y) of selected cell
                    TableColumnModel tcm = table.getColumnModel();
                    int rowIndex = table.rowAtPoint(e.getPoint());
                    int realRowIndex = rowIndex - 1;
                    int columnIndex = tcm.getColumnIndexAtX(e.getPoint().x);
                    int realColumnIndex = tcm.getColumn(columnIndex).getModelIndex() - 1;
                    String selectedFeatureName = realColumnIndex >= 0 ? columnToolTips.get(realColumnIndex) : null;
                    System.out.println(selectedFeatureName);
                    System.out.println("Metrics: ( " + rowIndex + ", " + columnIndex + " )");

                    // Get value of (x, y)
                    String value = "";
                    if (columnIndex > 0) {
                        value = table.getModel().getValueAt(rowIndex, columnIndex).toString();
                        System.out.println("value: " + value);
                    }

                    // Get feature name of selected row
                    Feature selectedFeature = features.get(rowIndex);

                    // Get feature locations
                    String selectedFeatureLocation = "";
                    ArrayList<Object> locationsForFeature = null;
                    List<String> foundList = new ArrayList<>();

                    try {
                        Map<String, ArrayList<Object>> featureMap = new HashMap<>();
                        locationsForFeature = iFeatureAnnotationUtil.getFeatureAnnotationLocations(project.getBasePath()).get(selectedFeature.name);

                        // Parse the locations format
                        String lff = locationsForFeature.toString();
                        String lffPattern = lff.replaceAll("(\\[)|(\\])","");
                        String[] splPattern = lffPattern.split(",");
                        for (String sp : splPattern) {
                            if (sp.contains(".java")){
                                foundList.add(sp);
                                System.out.println(sp);
                            }
                        }

                        featureMap.put(selectedFeature.name, locationsForFeature);
                        selectedFeatureLocation = JSONHandler.toJSONString(featureMap);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    System.out.println("selectedFeatureLocation: " + selectedFeatureLocation);

                    List<String> foundList1 = new ArrayList<>();
                    foundList1.add(selectedFeatureLocation);
                    JBPopupFactory.getInstance()
                            .createPopupChooserBuilder(foundList)
                            .setTitle("Selected Feature: " + selectedFeature.name)
                            .setMovable(true)
                            .setResizable(true)
                            .createPopup()
                            .showInScreenCoordinates(e.getComponent(), new Point(e.getXOnScreen(), e.getYOnScreen()));
                }
            }
        });

        // Table sorter
        sorter = new TableRowSorter<>(model);
        for (int i = 1; i < table.getColumnCount() - 1; i++) {
            sorter.setComparator(i, Comparator.comparingInt(x -> (int) x));
        }
        RowFilter<TableModel, Integer> emptyFilter = RowFilter.regexFilter("^\\s$", 0);
        RowFilter<TableModel, Integer> defaultFilter = RowFilter.notFilter(emptyFilter);
        sorter.setRowFilter(defaultFilter);
        table.setRowSorter(sorter);
        // Table cell renderer
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
            // override renderer preparation
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                           boolean hasFocus, int row, int column) {
                // allow default preparation
                super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

                // replace default font
                Font oldFont = getFont();
                if (isSelected)
                    setFont(oldFont.deriveFont(Font.BOLD, oldFont.getSize()+2));
                else
                    setFont(oldFont.deriveFont(Font.PLAIN));
                return this;
            }
        };
        for (int i = 0; i < table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(renderer);
        }
        // Add table to scroll pane
        JBScrollPane scrollPane = new JBScrollPane(table);

        // Filter TextField
        JBTextField filterText = new JBTextField();
        filterText.getEmptyText().setText("Filter features here");
        filterText.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            protected void textChanged(@NotNull DocumentEvent e) {
                String text = null;
                try {
                    text = e.getDocument().getText(0, e.getDocument().getLength());
                } catch (BadLocationException badLocationException) {
                    badLocationException.printStackTrace();
                }

                if (text != null && text.length() > 0) {
                    sorter.setRowFilter(RowFilter.regexFilter("(?i)" + text, 0));
                    // If nothing matches regex show an empty row
                    if (sorter.getViewRowCount() == 0) {
                        sorter.setRowFilter(emptyFilter);
                    }
                } else {
                    sorter.setRowFilter(defaultFilter);
                }
            }
        });

        // Full panel with all the content
        panel = FormBuilder.createFormBuilder()
                .addComponent(filterText, 0)
                .addComponentFillVertically(scrollPane, 0).getPanel();
    }

    /**
     * Gets the table of metrics
     * @return The table of metrics as a JComponent
     */
    public JComponent content() {
        return panel;
    }

    /**
     * Updates the table
     */
    public void update() {
        DefaultTableModel model = createModel(features);
        table.setModel(model);
        sorter.setModel(model);
        columnToolTips.clear();
        columnToolTips.addAll(createColumnToolTips(getTableMetrics()));
    }

    /**
     * Creates an array of all column names.
     *
     * @param metricsToAdd A list of all metrics that should be added to the table.
     * @return An array of column names.
     */
    private String[] createColumns(@NotNull List<Feature.Metric> metricsToAdd) {
        String[] arr = new String[metricsToAdd.size() + 1];
        arr[0] = "Feature";
        int count = 1;
        for (Feature.Metric metric : metricsToAdd) {
            arr[count] = metric.getDisplayName();
            count++;
        }
        return arr;
    }

    /**
     * Creates a list of all column header tooltips
     * @param metricsToAdd A list of all metrics that should be added to the table
     * @return A list of tooltips for the column headers
     */
    private ArrayList<String> createColumnToolTips(@NotNull List<Feature.Metric> metricsToAdd) {
        ArrayList<String> arr = new ArrayList<>(metricsToAdd.size());
        for (Feature.Metric metric : metricsToAdd) {
            switch (metric) {
                case SD:
                    arr.add("Scattering Degree");
                    break;
                case NOCA:
                    arr.add("Number of Code Annotations");
                    break;
                case NOFIA:
                    arr.add("Number of File Annotations");
                    break;
                case NOFOA:
                    arr.add("Number of Folder Annotations");
                    break;
                case TD:
                    arr.add("Tangling Degree");
                    break;
                case LOFC:
                    arr.add("Lines of Feature Code");
                    break;
            }
        }
        return arr;
    }

    /**
     * Creates the TableModel to be used by the metric table
     *
     * @param features The features to be displayed in the metric table
     * @return A TableModel with all features in it
     */
    private DefaultTableModel createModel(@NotNull List<Feature> features) {
        ArrayList<Feature.Metric> tableMetrics = getTableMetrics();
        final int NUM_COLUMNS = tableMetrics.size() + 1;

        Object[][] data = new Object[features.size()][NUM_COLUMNS];
        for (int i = 0; i < features.size(); i++) {
            int[] metrics = featureToMetricArray(features.get(i), tableMetrics);
            for (int j = 0; j < NUM_COLUMNS; j++) {
                if (j == 0) {
                    data[i][j] = features.get(i).name;
                    continue;
                }
                data[i][j] = metrics[j - 1];
            }
        }
        String[] columns = createColumns(tableMetrics);

        DefaultTableModel model = new DefaultTableModel(data, columns) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        // Empty row added for aesthetics when filtering with no match
        Object[] arr = new Object[NUM_COLUMNS];
        arr[0] = " ";
        model.addRow(arr);
        return model;
    }

    /**
     * Makes an array of a features metric sorted in display order
     *
     * @param feature      The feature whose metrics should be used
     * @param metricsToAdd The metrics to be added
     * @return An array of metrics
     */
    private int[] featureToMetricArray(@NotNull Feature feature, @NotNull List<Feature.Metric> metricsToAdd) {
        int[] arr = new int[metricsToAdd.size()];
        int count = 0;
        for (Feature.Metric metric : metricsToAdd) {
            switch (metric) {
                case SD:
                    arr[count] = feature.sd;
                    break;
                case NOCA:
                    arr[count] = feature.noca;
                    break;
                case NOFIA:
                    arr[count] = feature.nofia;
                    break;
                case NOFOA:
                    arr[count] = feature.nofoa;
                    break;
                case TD:
                    arr[count] = feature.td;
                    break;
                case LOFC:
                    arr[count] = feature.lofc;
                    break;
            }
            count++;
        }
        return arr;
    }

    //&begin[Settings]

    /**
     * Gets the order of a metric.
     *
     * @param metric   The metric whose order should be returned.
     * @param settings The state where metric order is stored.
     * @return Returns the order of the metric (as an int).
     */
    private int getMetricOrder(@NotNull Feature.Metric metric, @NotNull SettingsState settings) {
        switch (metric) {
            case SD:
                return settings.sdOrder;
            case NOCA:
                return settings.nocaOrder;
            case NOFIA:
                return settings.nofiaOrder;
            case NOFOA:
                return settings.nofoaOrder;
            case TD:
                return settings.tdOrder;
            case LOFC:
                return settings.lofcOrder;
            default:
                return -1;
        }
    }
    //&end[Settings]

    /**
     * Gets all metrics that should be shown in the table.
     *
     * @return A (ordered) list of metrics to be displayed in the table.
     */
    private ArrayList<Feature.Metric> getTableMetrics() {
        ArrayList<Feature.Metric> metrics = new ArrayList<>();
        //&begin[Settings]
        SettingsState settings = SettingsState.getInstance();
        if (isTableMetric(settings.sd))
            metrics.add(Feature.Metric.SD);
        if (isTableMetric(settings.noca))
            metrics.add(Feature.Metric.NOCA);
        if (isTableMetric(settings.nofia))
            metrics.add(Feature.Metric.NOFIA);
        if (isTableMetric(settings.nofoa))
            metrics.add(Feature.Metric.NOFOA);
        if (isTableMetric(settings.td))
            metrics.add(Feature.Metric.TD);
        if (isTableMetric(settings.lofc))
            metrics.add(Feature.Metric.LOFC);
        //&end[Settings]
        metrics.sort((Feature.Metric m1, Feature.Metric m2) -> {
            //&begin[Settings]
            int i1Order = getMetricOrder(m1, settings);
            int i2Order = getMetricOrder(m2, settings);
            //&end[Settings]
            return Integer.compare(i1Order, i2Order);
        });
        return metrics;
    }

    /**
     * Checks whether or not a metric should be displayed on the table
     *
     * @param metric The metric which is to be determined whether it should be displayed.
     * @return True if it should be displayed, else false.
     */
    private boolean isTableMetric(int metric) {
        return (metric == 0 || metric == 1);
    }
}
