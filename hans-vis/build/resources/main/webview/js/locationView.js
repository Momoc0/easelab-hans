let chartData;
let resBase64;
let selElement;

function hideMenu() {
    document.getElementById("contextMenu")
        .style.display = "none"
}

function rightClick(param) {
    if (document.getElementById("contextMenu").style.display === "block") {
        hideMenu();
    } else {
        let e = param.event;
        let menu = document.getElementById("contextMenu");
        menu.style.display = "block";
        menu.style.left = e.offsetX + "px";
        menu.style.top = e.offsetY + 20 + "px";
    }
}


/**
 * adapt the response data (feature-to-folder) to specific JSONString which in format:
 {
        "name": "root",
        "children": [{
            "name": "sub-root",
            "children": [{
				"name": 'attribute1',
				"value": '1'
			},{
				"name": 'attribute2',
				"value": '1'
			},{
				"name": 'attribute3',
				"value": '1'
			},{
				"name": 'attribute4',
				"value": '1'
			}]
       	}]
    }
 * @param resData
 * @returns {{symbol: string, name: string, itemStyle: {color: string}, value: string}}
 */
function parsingData(resData) {
    let parsedData = JSON.parse(resData);
    let parentArr = [];
    let dataTree = {
        "name": "ffo",
        "value": '0',
        "symbol": "circle",
        "itemStyle": {
            color: "#d0061e"
        },
    };

    for (let k in parsedData) {
        // console.log("[name]: " + k + " [value]: " + JSON.stringify(parsedData[k]));
        let v = String(parsedData[k]).replace("[", "").replace("]", "");
        let childArr = [];
        let parentNode = {
            "name": k,
            "value": '1',
            "symbol": "circle",
            "itemStyle": {
                color: "#d00695"
            },
        };
        let childNode = {
            "name": v,
            "value": '2',
            "symbol": "circle",
            "label": {
                width: 100,
                height: 40,
                lineHeight: 40,
                position: 'right',
                verticalAlign: 'middle',
                align: 'center',
                rich: {
                    img1: {
                        backgroundColor: {
                            image: "../images/FolderIcon2.svg",
                        },
                        height: 40
                    }
                },
                formatter: function (param) {
                    let res = "";
                    res += '{img1|}' + param.name;
                    return res;
                }
            }
        }
        // console.log("******************************************************");
        childArr.push(childNode);
        parentNode.children = childArr;
        parentArr.push(parentNode);
    }
    // console.log("[parentArr]: "+JSON.stringify(parentArr));
    dataTree.children = parentArr;
    // console.log("[dataTree]: "+JSON.stringify(dataTree));
    return dataTree;
}

/**
 * Print the current location view and then refresh the page
 */
function printImage() {
    $("#snapshot").attr("src", resBase64).show(); // canvas rendering by ajax
    setTimeout(function () {
        // partial printing
        bdhtml = window.document.body.innerHTML; // get the whole html page
        sprnstr = "<!--start-print-->"; // begin of section
        eprnstr = "<!--end-print-->"; // end of section
        prnhtml = bdhtml.substr(bdhtml.indexOf(sprnstr) + 18);
        prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));

        window.document.body.innerHTML = prnhtml; // preview the printed section in html page
        window.print(); // start print
        location.reload(); // page refresh after printing
    }, 3500);
}

/**
 *  Add tree node.
 */
function addNode() {
    let selValue = selElement.value;
    let nodeName = "";
    let nodeFolderName = "";

    if (selValue !== "0") {
        alert("Please select root node if you want to add a new one.");
        return;
    }

    nodeName = prompt("Node name ?");
    nodeFolderName = prompt("Please enter folder location.");
    let options = chartData.getOption();
    let nodesOption = options.series[0].data;

    // create new node and its folder location
    //****************************************
    let childArr = [];
    let parentNode = {
        "name": nodeName,
        "value": '1',
        "symbol": "circle",
        "itemStyle": {
            color: "#d00695"
        },
    };
    let childNode = {
        "name": nodeFolderName,
        "value": '2',
        "symbol": "circle",
        "label": {
            width: 100,
            height: 40,
            lineHeight: 40,
            position: 'right',
            verticalAlign: 'middle',
            align: 'center',
            rich: {
                img1: {
                    backgroundColor: {
                        image: "../images/FolderIcon2.svg",
                    },
                    height: 40
                }
            },
            formatter: function (param) {
                let res = "";
                res += '{img1|}' + param.name;
                return res;
            }
        }
    }
    childArr.push(childNode);
    parentNode.children = childArr;
    nodesOption[0].children.push(parentNode);
    //****************************************

    chartData.setOption(options);
}

/**
 *  Rename tree node.
 */
function renameNode() {
    let selName = selElement.data.name;
    let selValue = selElement.value;
    console.log("[selName] " + selName + ", [selValue] " + selValue);

    if (selValue === "0") {
        alert("Root cannot be renamed.");
        return;
    }
    let nodeName=prompt("Enter a new name please.");
    let options = chartData.getOption();
    let nodesOption = options.series[0].data;

    for (let m of nodesOption[0].children) {
        if (m.name === selName) {
            m.name = nodeName; // rename the selected node from children list.
            break;
        }
    }
    chartData.setOption(options);
}


/**
 *  Delete tree node.
 */
function deleteNode() {
    let res = confirm("Delete this node?");
    if (res) {
        let selName = selElement.data.name;
        let selValue = selElement.value;
        console.log("[selName] " + selName + ", [selValue] " + selValue);

        if (selValue === "0") {
            alert("Root cannot be deleted.");
            return;
        }
        let options = chartData.getOption();
        let nodesOption = options.series[0].data;
        let childArr = nodesOption[0].children;

        console.log("in loop, type " + typeof nodesOption[0].children + " , " +
            typeof nodesOption[0].children[0].name);

        let count = 0; // index counter
        for (let m of nodesOption[0].children) {
            console.log("in loop, type " + typeof m + " , " + JSON.stringify(m.name));
            if (m.name === selName) {
                console.log("count " + count);
                nodesOption[0].children.splice(count, 1)// remove the selected node from children list.
                break;
            }
            count++;
        }
        chartData.setOption(options);
    }
}

/**
 * Create and configure echarts instance.
 * @param DOMName
 * @param JSONData
 */
function createEcharts(DOMName, JSONData) {
    // initialize an instance of echarts
    let myChart = echarts.init(document.getElementById(DOMName));
    const data = JSONData;

    let option = {
        tooltip: {
            trigger: 'item',
            triggerOn: 'mousemove'
        },
        title: {
            text: 'Feature-to-Folder',
            textStyle: {
                fontSize: 20,
                color: "#d04606"
            },
        },
        series: [
            {
                type: 'tree',
                id: 0,
                name: 'tree1',
                data: [data],

                top: '10%',
                left: '8%',
                bottom: '22%',
                right: '20%',
                symbolSize: 10, // original: 7
                // roam: "scale",

                edgeShape: 'polyline',// option: 'polyline' or 'curve'
                edgeForkPosition: '63%',
                initialTreeDepth: 3,

                lineStyle: {
                    width: 2
                },

                label: {
                    backgroundColor: '#fff',
                    position: 'left',
                    verticalAlign: 'middle',
                    align: 'right',
                    fontSize: 15
                },

                leaves: {
                    label: {
                        position: 'right',
                        verticalAlign: 'middle',
                        align: 'left'
                    }
                },

                expandAndCollapse: true,
                animationDuration: 550,
                animationDurationUpdate: 750
            }
        ]
    };
    let opts = {
        type: "png", // option: png, jpeg
        pixelRatio: 1,// default value: 1
        // backgroundColor: '#fff'
    }
    myChart.setOption(option);
    chartData = myChart;
    myChart.on('contextmenu', function (param) { // options: 'click' / 'dblclick' / 'contextmenu'
        selElement = param;
        document.getElementById("entrance").oncontextmenu = function () {
            return false; // disable the default right-click menu
        };
        document.onclick = hideMenu;
        rightClick(param);
    });
    setTimeout(function () {
        resBase64 = myChart.getDataURL(opts);
    }, 500);
}
